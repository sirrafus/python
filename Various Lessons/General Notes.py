# ctrl + /  - comment uncomment
# ctrl + numpad +  - expand block or collapse
# alt + shift + enter reformat code
# everything in python is an object & you can use individual functions or methods to add or remove items to that object
# when the function is part of an object it is called method
# ctrl + space to show possible values autocomplete
# dir shows the attributes of the given object