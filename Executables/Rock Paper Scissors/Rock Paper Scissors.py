import random
rock = '''
ROCK
    _______
---'   ____)
      (_____)
      (_____)
      (____)
---.__(___)
'''

paper = '''
PAPER
    _______
---'   ____)____
          ______)
          _______)
         _______)
---.__________)
'''

scissors = '''
SCISSORS
    _______
---'   ____)____
          ______)
       __________)
      (____)
---.__(___)
'''

# Write your code below this line

choice = int(input("What do you choose? Type 0 for Rock, 1 for Paper or 2 for Scissors\n"))
options = [rock, paper, scissors]

possible_answer = random.randint(0, 2)

comp_answer = options[possible_answer]
# you can print your (user's choice outside of if, as it will be printer anyway, but this way it will not be printed
# on wrong inputs
# you can take main if to the very top and all the rest can be under else, with one indent
if choice == 0:
    print("You chose:\n")
    if comp_answer == options[0]:  # just to show option of prints in one line
        print(options[0], f"Computer chose:\n{comp_answer}", "Draw, try again")
    elif comp_answer == options[1]:
        print(options[0])
        print(f"Computer chose:\n{comp_answer}")
        print("You lose :(")
    elif comp_answer == options[2]:
        print(options[0])
        print(f"Computer chose:\n{comp_answer}")
        print("You win!")
elif choice == 1:
    print("You chose:\n")
    if comp_answer == options[0]:
        print(options[0])
        print(f"Computer chose:\n{comp_answer}")
        print("You win!")
    elif comp_answer == options[1]:
        print(options[0])
        print(f"Computer chose:\n{comp_answer}")
        print("Draw, try again")
    elif comp_answer == options[2]:
        print(options[0])
        print(f"Computer chose:\n{comp_answer}")
        print("You lose :(")
elif choice == 2:
    print("You chose:\n")
    if comp_answer == options[0]:
        print(options[0])
        print(f"Computer chose:\n{comp_answer}")
        print("You lose :(")
    elif comp_answer == options[1]:
        print(options[0])
        print(f"Computer chose:\n{comp_answer}")
        print("You win!")
    elif comp_answer == options[2]:
        print(options[0])
        print(f"Computer chose:\n{comp_answer}")
        print("Draw, try again")
else:
    print("Invalid input, You lose")

# alternative by Angela, to cover all options with one bigger block
# uses general logic that if your input is bigger, you usually win unless it's too big number or too small
# scissors > paper > rock however rock > scissors
# note that in this case we need to int() comp answer as it directly works in comparison instead of
# e.g. comp_answer == options [0] like in my version
# if 3 >= choice or 0 > choice:   # or if choice >= 3 or choice < 0:
#     print("wrong input, you lose")
# elif choice == 0 and comp_answer == 2:
#     print("You win")
# elif comp_answer == 2 and choice == 0:
#     print("you lose")
# elif int(comp_answer) > choice:
#     print("you lose")
# elif choice > int(comp_answer):
#     print("You win")
# elif int(comp_answer) == choice:
#     print("Draw")
#
input("")