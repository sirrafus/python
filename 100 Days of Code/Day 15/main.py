import time

MENU = {
    "espresso": {
        "ingredients": {
            "water": 50,
            "coffee": 18,
        },
        "cost": 1.5,
    },
    "latte": {
        "ingredients": {
            "water": 200,
            "milk": 150,
            "coffee": 24,
        },
        "cost": 2.5,
    },
    "cappuccino": {
        "ingredients": {
            "water": 250,
            "milk": 100,
            "coffee": 24,
        },
        "cost": 3.0,
    }
}

resources = {
    "water": 300,
    "milk": 200,
    "coffee": 100,
}

money = 0
should_stop = False
coin_values = {"quarter": 0.25, "dime": 0.1, "nickel": 0.05, "penny": 0.01}


def coin_collector():
    print("Please insert coins")
    coins = {
        "quarter": int(input("How many quarters?: ")),
        "dime": int(input("How many dimes?: ")),
        "nickel": int(input("How many nickles?: ")),
        "penny": int(input("How many pennies?: "))
    }
    return coins


def calculator(total):
    total_value = 0
    for coin in total:
        value = total[coin] * coin_values[coin]
        total_value += value
    return total_value


def coffee(requested_coffee):
    if requested_coffee == "latte" or requested_coffee == "cappuccino":
        water_required = MENU[request]['ingredients']['water']
        milk_required = MENU[request]['ingredients']['milk']
        coffee_required = MENU[request]['ingredients']['coffee']
        water_available = resources['water']
        milk_available = resources['milk']
        coffee_available = resources['coffee']
        if water_required > water_available:
            print("Not enough water in the machine.")
            return "skip coins"
        elif milk_required > milk_available:
            print("Not enough milk in the machine.")
        elif coffee_required > coffee_available:
            print("Not enough coffee in the machine.")
            return "skip coins"
        else:
            resources["water"] -= MENU[requested_coffee]['ingredients']["water"]
            resources["milk"] -= MENU[requested_coffee]['ingredients']["milk"]
            resources["coffee"] -= MENU[requested_coffee]['ingredients']["coffee"]

    elif requested_coffee == "espresso":

        water_required = MENU[request]['ingredients']['water']
        coffee_required = MENU[request]['ingredients']['coffee']
        water_available = resources['water']
        coffee_available = resources['coffee']
        if water_required > water_available:
            print("Not enough water in the machine.")
            return "skip coins"
        elif coffee_required > coffee_available:
            print("Not enough coffee in the machine.")
            return "skip coins"
        else:
            resources["water"] -= MENU[requested_coffee]['ingredients']["water"]
            resources["coffee"] -= MENU[requested_coffee]['ingredients']["coffee"]

    elif requested_coffee == 'report':
        print(f"Water: {resources['water']}ml")
        print(f"Milk: {resources['milk']}ml")
        print(f"Coffee: {resources['coffee']}g")
        print(f"Money: ${money}")


while not should_stop:
    request = input("What would you like? (espresso/latte/cappuccino): ")
    if request == "off":
        print("Shutting down...")
        time.sleep(5)
        should_stop = True
    elif request != 'report':
        price = MENU[request]['cost']
        if coffee(request) != "skip coins":
            total_coins = coin_collector()
            given_money = calculator(total_coins)
            diff = round(given_money - price, 2)
            if diff < 0:
                print(f"Sorry ${given_money} is not enough. Money refunded.")
            elif diff == 0:
                print(f"Here is your {request} ☕ . Enjoy!")
                money += price
            elif diff > 0:
                print(f"Here is ${diff} in change.")
                money += price
                print(f"Here is your {request} ☕ . Enjoy!")
    else:
        coffee(request)
