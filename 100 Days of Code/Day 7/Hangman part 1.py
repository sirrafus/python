# Step 1
import random

word_list = ["aardvark", "baboon", "camel"]

# TO-DO-1 - Randomly choose a word from the word_list and assign it to a variable called chosen_word.

# TO-DO-2 - Ask the user to guess a letter and assign their answer to a variable called guess. Make guess lowercase.

# TO-DO-3 - Check if the letter the user guessed (guess) is one of the letters in the chosen_word.

random_word = random.choice(word_list)
print(random_word)
guess = input("Guess a letter: ").lower()

for letter in random_word:
	if letter == guess:  # or guess == char to check only if it's there or not, not just type right wrong per letter
		print("Right")
	else:
		print("Wrong")
