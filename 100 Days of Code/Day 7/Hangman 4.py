import random

stages = ['''
  +---+
  |   |
 x_x  |
 /|\  |
 / \  |
      |
=========
''', '''
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========
''', '''
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========
''', '''
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
  |   |
      |
      |
=========
''', '''
  +---+
  |   |
  O   |
      |
      |
      |
=========
''', '''
  +---+
  |   |
      |
      |
      |
      |
=========
''']

end_of_game = False
word_list = ["aardvark", "baboon", "camel",
             "the", "of", "and", "a", "to", "in", "is", "you", "that", "it", "he", "was",
             "for", "on", "are", "as", "with", "his", "they", "I", "at", "be", "this", "have",
             "from", "or", "one", "had", "by", "word", "but", "not", "what", "all", "were", "we",
             "when", "your", "can", "said", "there", "use", "an", "each", "which", "she", "do",
             "how", "their", "if", "will", "up", "other", "about", "out", "many", "then", "them",
             "these", "so", "some", "her", "would", "make", "like", "him", "into", "time", "has",
             "look", "two", "more", "write", "go", "see", "number", "no", "way", "could",
             "people", "my", "than", "first", "water", "been", "call", "who", "oil", "its",
             "now", "find", "long", "down", "day", "did", "get", "come", "made", "may", "part"]
chosen_word = random.choice(word_list)
word_len = len(chosen_word)
lives = 6

# Testing code
print(f'Pssst, the solution is {chosen_word}.')

# Create blanks

display = []
for _ in range(word_len):
    display += "_"

while not end_of_game:
    counter = int()
    guess = input("Guess a letter: ").lower()
    # Check guessed letter
    for position in range(word_len):
        letter = chosen_word[position]
        if letter != guess:
            counter += 1
            if counter == word_len:
                lives -= 1
                print(stages[lives])
        else:
            display[position] = letter
    print(display)
    if "_" not in display:
        print("You win!")
        end_of_game = True
    elif lives == 0:
        print("You Lose...")
        end_of_game = True

# Join all the elements in the list and turn it into a String.
print(f"\nThe hidden word was {''.join(chosen_word).upper()}")
