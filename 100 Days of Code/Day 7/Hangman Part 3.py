import random

print("""
88                                                                            
88                                                                            
88                                                                            
88,dPPYba,  ,adPPYYba, 8b,dPPYba,   ,adPPYb,d8 88,dPYba,,adPYba,  ,adPPYYba, 8b,dPPYba, 
88P'    "8a ""     `Y8 88P'   `"8a a8"    `Y88 88P'   "88"    "8a ""     `Y8 88P'   `"8a
88       88 ,adPPPPP88 88       88 8b       88 88      88      88 ,adPPPPP88 88       88
88       88 88,    ,88 88       88 "8a,   ,d88 88      88      88 88,    ,88 88       88
88       88 `"8bbdP"Y8 88       88  `"YbbdP"Y8 88      88      88 `"8bbdP"Y8 88       88
                                    aa,    ,88                                
                                     "Y8bbdP"                                 
""")

word_list = ["aardvark", "baboon", "camel"]
chosen_word = random.choice(word_list)
word_len = len(chosen_word)

display = []

for _ in range(word_len):
    display += "_"

while "_" in display:   # alternatively you can create end of game var set it to false and check after each while using
                        # using if switch, which will change to true
    guess = input("Guess a letter: ").lower()
    for position in range(word_len):
        letter = chosen_word[position]
        if letter == guess:
            display[position] = guess
    print(display)

display_str = ""
for char in display:
    display_str += char

else:
    print("You win!")
    print(f"Full word is: {str(display_str)}")
