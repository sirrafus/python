import random
import os
from words import word_list

print("""
88                                                                            
88                                                                            
88                                                                            
88,dPPYba,  ,adPPYYba, 8b,dPPYba,   ,adPPYb,d8 88,dPYba,,adPYba,  ,adPPYYba, 8b,dPPYba, 
88P'    "8a ""     `Y8 88P'   `"8a a8"    `Y88 88P'   "88"    "8a ""     `Y8 88P'   `"8a
88       88 ,adPPPPP88 88       88 8b       88 88      88      88 ,adPPPPP88 88       88
88       88 88,    ,88 88       88 "8a,   ,d88 88      88      88 88,    ,88 88       88
88       88 `"8bbdP"Y8 88       88  `"YbbdP"Y8 88      88      88 `"8bbdP"Y8 88       88
                                    aa,    ,88                                
                                     "Y8bbdP"                                 
""")

stages = ['''
  +---+
  |   |
 x_x  |
 /|\  |
 / \  |
      |
=========
''', '''
  +---+
  |   |
  O   |
 /|\  |
 /    |
      |
=========
''', '''
  +---+
  |   |
  O   |
 /|\  |
      |
      |
=========
''', '''
  +---+
  |   |
  O   |
 /|   |
      |
      |
=========''', '''
  +---+
  |   |
  O   |
  |   |
      |
      |
=========
''', '''
  +---+
  |   |
  O   |
      |
      |
      |
=========
''', '''
  +---+
  |   |
      |
      |
      |
      |
=========
''']

end_of_game = False
chosen_word = random.choice(word_list)
word_len = len(chosen_word)
lives = 6

display = []
for _ in range(word_len):
    display += "_"

while not end_of_game:
    guess = input("Guess a letter: ").lower()
    # os.system('cls')
    if guess in display:
        print(f"You've already guessed the letter {guess}")

    # Check guessed letter
    for position in range(word_len):
        letter = chosen_word[position]
        # print(f"Current position: {position}\n
        # Current letter: {letter}\n
        # Guessed letter: {guess}")
        if letter == guess:
            display[position] = letter
    print(f"{''.join(display)}")
    # for loop exits after checking all positions in range and goes down then goes to while again
    if guess not in chosen_word:
        print(f"Letter {guess} is not in the target word, you lose a life")
        lives -= 1
        if lives == 0:
            print("You Lose...")
            end_of_game = True
    if "_" not in display:
        print("You win!")
        end_of_game = True
    print(stages[lives])
# Join all the elements in the list and turn it into a String.
print(f"\nThe target word was {''.join(chosen_word).upper()}")
