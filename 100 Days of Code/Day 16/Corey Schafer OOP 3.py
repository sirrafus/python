"""
Regular methods automatically take instance (self) as a first argument
to make it take Class as instance we can create Class Methods

"""
import datetime
import inspect


class Employee:
    raise_amount = 1.04  # class variable.
    # Even within the class you should call it either by class name or instance name self
    num_of_employees = 0  # to count every employee that was created with a class

    def __init__(self, first, last, pay):  # we can create email using first and last
        self.first = first
        # you can name attributes different from parameters, but it's better keep team similar
        self.last = last
        self.pay = int(pay)
        self.email = f"{first}.{last}@noctis.am"

        Employee.num_of_employees += 1  # not self because it should be changed on any instance not for certain one

    def full_name(self):
        return f"Full name is {self.first} {self.last}"

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)

    @classmethod
    def set_raise_amt(cls, amount):  # class as an argument (cls is common convention)
        cls.raise_amount = amount  # equals amount that we are accepting in the method

    @classmethod
    def from_string(cls, emp_str):  # alternative contractors usually have convention to start with "from"
        first, last, pay = emp_str.split("-")
        return cls(first, last, int(pay))

    @staticmethod
    def is_workday(day):
        if day.weekday() == 5 or day.weekday() == 6:
            return False
        return True


employee_1 = Employee("Mandr", "Mundr", 7800)
employee_2 = Employee("Tigran", "Melikyan", 10000)
print(Employee.num_of_employees)

Employee.set_raise_amt(1.05)  # same as asking to add like this Employee.raise_amount = 1.05
# even if you this for only 1 instance it will change for the whole class so this doesn't make sense to use

print(Employee.raise_amount)  # all 3 will print 1.04 before but change to 1.05 because of the like 39
print(employee_1.raise_amount)
print(employee_2.raise_amount)

"""
In some cases in real  life there might be situations that we have data to proceed formatted differently.
"""

emp_str_1 = "Tigran-Melikyan-10000"
# user can obviously parse this string and get it the way he wants like this
first, last, pay = emp_str_1.split('-')
new_emp_1 = Employee(first, last, pay)
print(new_emp_1.email)

# however, instead we can create alternative constructor for a class (not __init__)

emp_str_2 = "Ruzanna-Melikyan-7800"
emp_str_3 = "Dishwasher-Mughdusyan-500"

"""
To check all what's in the module use inspect
import datetime
import inspect

datetime_ispector = inspect.getmembers(datetime)
print(datetime_ispector)
"""

# not exactly working as expected, will look into this later
new_emp_2 = Employee.from_string(emp_str_2)
new_emp_3 = Employee.from_string(emp_str_3)

print(new_emp_3)

# staticmethods are just regular functions that don't take instance of a class, but are related to it so we included
# it in the class code not outside, because it has some logical connection with the class

day = datetime.date.today()
print(day)
day_to_check = Employee.is_workday(day)

print(day_to_check)