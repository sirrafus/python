# Procedural programming is basically when code just runs from top to bottom, jumping back and forth
# from function to function as needed

# Analogy
# we run a restaurant (class is restaurant), and you are limited on how many seats you can have, i
# f you run everything alone however if you have manager you can hire shed, waiter and cleaner so each can cover
# their part to model waiter for example we should model what he has and what he does

# object is waiter

# Object is way of combining data with some functionality

"""
attributes are variables that associated with modeled object (a waiter)
things that object has -  is_holding_plate = True , tables_responsible = [4,5,6]

A parameter is the variable listed inside the parentheses in the function definition.

An argument is the value that are sent to the function when it is called.

def function(parameter)
    bla bla

function(parameter = argument)

methods are functions that a particular modeled object (waiter) can do
def take_order(table, order)
waiter takes order to chef
def take_payment(amount)
waiter adds money to restaurant

Object is the way of combining some piece of data and some functionality

we can have multiple object generated from the same type (blueprint) which is the class

so blueprint of car generates objects (a car itself)
that have methods (things it can do like break or accelerate) and attributes (wheels = 4)

class CarBlueprint:

car has multiple functionalities like how can it drive and break.
also, it has specification - how many wheels, color etc
combined they are class for car

classes usually have first letter of each word capitalized, called Pascal case

car = CarBlueprint()
"""

from turtle import Turtle, Screen


"""
importing class (blueprint of how to create object) Turtle from module turtle (book of blueprints)

parentheses in the and means construct(create) an object using mentioned class

"""


yuro = Turtle()  # or yuro = Turtle() if you already imported Turtle class by itself (from turtle import Turtle
# yuro is the turtle object
print(yuro)
# will print object and the place in the memory it takes like 0x00001D25F
# to use attributes of an object you should use . car.speed (car is object and speed is attribute
# shape is attribute
yuro.shape('turtle')
yuro.color("DarkTurquoise")

yuro.forward(100)  # pixels to move
yuro.right(90)  # angle
yuro.forward(100)
yuro.right(90)  # angle
yuro.forward(100)
yuro.right(90)  # angle
yuro.forward(100)
yuro.right(90)  # angle

# my screen is the screen that shows how's yuro actually running instead of background

my_screen = Screen()
my_screen.exitonclick()

# # forward is method
# # basically we are taking the object and calling function for that object
# # passing 100 as an argument for parameter distance


