"""
Class Variables are variables that are shared (are same) among all instances.
instance variables contain data that is unique for each instance
We should think what data should be shared between all employees.
For example they all should get same amount of raise
"""


class Employee:
    raise_amount = 1.04     # class variable.
    # Even within the class you should call it either by class name or instance name self
    num_of_employees = 0    # to count every employee that was created with a class

    def __init__(self, first, last, pay):   # we can create email using first and last
        self.first = first
        # you can name attributes different from parameters, but it's better keep team similar
        self.last = last
        self.pay = pay
        self.email = f"{first}.{last}@noctis.am"

        Employee.num_of_employees += 1 # not self because it should be changed on any instance not for certain one

    def full_name(self):
        return f"Full name is {self.first} {self.last}"

    def apply_raise(self):
        self.pay = int(self.pay * self.raise_amount)


employee_1 = Employee("Mandr", "Mundr", 7800 )
# you can call class variable using both instance or class itself
print(Employee.raise_amount)
print(employee_1.raise_amount)


# if you print __dict__ special variable for a method you will see the data inside
# note that it will not contain class var
print(employee_1.__dict__)

# when you print __dict__ for class itself you will see it inside all the objects of it
print(Employee.__dict__)

# Also, you can change class var for 1 instance and for whole class
employee_1.raise_amount = 1.05


print(Employee.raise_amount)        # will print the old value 1.04
print(employee_1.raise_amount)      # will print the new value 1.05

Employee.raise_amount = 1.05
print(Employee.raise_amount)        # will print the new value 1.05
print(employee_1.raise_amount)      # will print the new value 1.05

# after change if you print __dict__ for employee_1 you will raise amount is now there because you manually added it
# and when you apply raise method if it has variable inside of method, it will be applied
print(employee_1.__dict__)

# to check how many employees we created so far
print(Employee.num_of_employees)