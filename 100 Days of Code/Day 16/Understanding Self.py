# class employee with only 1 method - set salary
class Employee1:
    def set_salary(self,value):
        self.salary = value


"""creating instance of class employee
set salary has 2 parameters, but we are passing only 1 argument because
set salary is an attribute that holds a function object
e is a class instance
when you call instance e python searches for it's class. we said the class is Employee
after that python searches in the class for a function object set_salary that changes salary with the passed 
value of 2000
e.set_salary is the method object which python calls with argument 2000
new argument list of 2 and 2000 is returned

In other words:
when you call a class method e.set_salary 
python maps this method call to a function call in an object and automatically ads instance as it's 1st argument (self)

YOU DON'T NEED TO PROVIDE SELF WHEN CALLING A CLASS INSTANCE
"""
e = Employee1()
e.set_salary(2000)
print(e.salary)


"""
First, we declare the class Dog using the keyword class. We use the keyword def to define a function or method, 
such as the __init__ method. As you can see, the __init__ method initializes two attributes: breed and eyeColor. 

We’ll now see how to pass these parameters when declaring an object. This is where we need the keyword self to bind 
the object’s attributes to the arguments received.
"""


class Dog:

    def __init__(self, dogbreed, dogeyecolor):
        self.breed = dogbreed
        self.eyeColor = dogeyecolor

"""
The __init__ method uses the keyword self to assign the values passed as arguments to the object
 attributes self.breed and self.eyeColor.
"""
tomita = Dog("Fox", "brown")
print(tomita.breed)


# Another example
class Employee:
    def __init__(self, first_name, last_name):
        self.f_name = first_name
        self.l_name = last_name

    def get_full_name(self):
        return f"{self.f_name} {self.l_name}".title()


vzgo = Employee("vazgen", "manukyan")
print(vzgo.get_full_name())

