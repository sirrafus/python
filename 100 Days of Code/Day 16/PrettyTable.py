# importing class from lib(module)
from prettytable import PrettyTable, from_db_cursor
import sqlite3
table = PrettyTable()

table.add_column("Pokemon names", ["Pickachu", "Squirtle", "Charmander"], "l", "b")
table.add_column("Type", ["Electric", "Water", "Fire"], "r")
print(table)

# to get the alignment of table
print(table.align)

table.align = 'l'
connection = sqlite3.connect("chinook.db")
cursor = connection.cursor()
cursor.execute("SELECT Title FROM albums")
mytable = from_db_cursor(cursor)
print(mytable)