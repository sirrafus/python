"""
Classes are the best way to provide creation of multiple objects with similar blueprints for example company
Employee_tests.

Method is a function that is associated with a class

each Employee_test that we will crate using blueprint of a class Employee_test would be instanced of Employee_test class

instance variables contain data that is unique for each instance

Class names should be CamelCase

"""


class EmployeeTest:
    pass


# doing it manually

emp_1 = EmployeeTest()
emp_2 = EmployeeTest()

print(emp_1)
print(emp_2)

emp_1.first = "Tigran"
emp_1.last = "Melikyan"
emp_1.email = "tigranmelikyan@gmail.com"
emp_1.pay = 10000

emp_2.first = "Gyoja"
emp_2.last = "Manryan"
emp_2.email = "pichxa@gmail.com"
emp_2.pay = 7800

print(emp_1.email)
print(emp_2.email)


# init method is initializer (constructor) is a special method
# when we create methods within a class, they receive the instance as a first argument automatically.
# by convention that first argument should be called self
# after self we need to specify what arguments should or class receive to start constructing
# if you forget to add self you will get type error method takes n positional arguments but n+1 was given

class Employee:
    def __init__(self, first, last, pay):   # we can create email using first and last
        self.first = first
        # you can name attributes different from parameters, but it's better keep team similar
        self.last = last
        self.email = f"{first}.{last}@noctis.am"
        self.pay = pay

    def full_name(self):
        return f"Full name is {self.first} {self.last}"


# now to create new objects with the class we created we should instantiate the class
employee_1 = Employee("Tigran", "Melikyan", 10000)
employee_2 = Employee("Ruzanchik", "Melikyan", 7800)

print(employee_1.email)
print(employee_2.email)

# obviously we can join for example first and last names like below, but instead we can add methods to our class
print(f"{employee_1.first} {employee_1.last}")

print(employee_1.full_name())   # we need parentheses in the end of method and not an attribute

# when you running method without instantiating, you should pass self manually like this
# the way we do below is called "Calling method on a class"
print(Employee.full_name(employee_1))
# below row does the same but because you already initiated it with employee1. you don't have to pass self again
# below you call a method using instance so we already have self
print(employee_1.full_name())
