# all functions use parentheses
# creating you own function
# colon means everything that comes after that and is indented, belongs to that function
# there should be at least 2 lines after aa function def block as epr PEP8

def my_function():
    print("Hello")      # do this
    print("Bye")        # do that


my_function()  # calling the function
# after function is done python goes back to where it left before and continues lines one by one
