def right():
    for i in range(0, 3):
        turn_left()
    move()

while not at_goal():
    if right_is_clear():
        right()
    elif front_is_clear():
        move()
    else:
        turn_left()
################################################################
# WARNING: Do not change this comment.
# Library Code is below.
################################################################
# problem 1

right_count = 0
def right():
    for i in range(0, 3):
        turn_left()
    move()
    return right_count +1

while not at_goal():
    if right_is_clear():
        right_count = right()
        if right_count == 4:
            turn_left()
    elif front_is_clear():
            move()
    else:
        turn_left()

