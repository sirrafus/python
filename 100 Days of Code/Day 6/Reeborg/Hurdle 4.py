def turn_right():
    for i in range(0, 3):
        turn_left()

def up():
    height = 0
    while wall_on_right():
        move()
        height += 1
    else:
        turn_right()
        move()
        turn_right()
        for step in range(0, height):
            move()
        turn_left()
   
while not at_goal():
    if front_is_clear():
       move()
    else:
        turn_left()
        up()

################################################################
# WARNING: Do not change this comment.
# Library Code is below.
################################################################
