# loops that continue as long as condition is true
# for loops have 2 main ways to run - 1 as long as given value's items end and when given range ends
# following loop will run 6 times and stop as it starts from 0 and after 5th time it will and qty will be equal to 6
# https://reeborg.ca/reeborg.html

qty_of_hurdles = 0
while qty_of_hurdles < 6:
	print("test")
	qty_of_hurdles += 1

# you can do the same by substracting 1 per each loop

while qty_of_hurdles > 0:
	print("test2")
	qty_of_hurdles -= 1
