# default indentation is 1 tab or 4 spaces
# do not break backwards compatibility just to comply with PEP styling
# there cannot be empty function without anything
def sky_color(sky_color):
    if sky_color == "blue":
        print("it's clear")
    elif sky_color == "grey":
        print("it's cloudy")
    else:
        print("Wrong input")

sky_color("sev")

