# global scope

enemies = 1


def increase_enemies():
    enemies = 2  # local scope
    print(f"enemies inside function: {enemies}")


increase_enemies()
print(f"enemies outside function: {enemies}")

# local scope exists within the functions


game_level = 3
enemies = ["Skeleton", "Zombie", "Alien"]
if game_level < 5:
    new_enemy = enemies[0]

print(new_enemy)


# we can change the variable scopy by explicitly saying it's global or local etc
enemies = 1


def increase_enemies():
    global enemies
    enemies += 1  # local scope
    print(f"enemies inside function: {enemies}")


increase_enemies()
print(f"enemies outside function: {enemies}")

enemies = 1


def increase_enemies():
    return enemies + 1  # local scope


increase_enemies()
print(f"enemies outside function: {enemies}")