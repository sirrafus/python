# numbers = []
# for number in range(1, 101):
#     numbers.append(number)
# print(numbers)
import random


NUMBERS = [
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25,
    26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
    51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75,
    76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100
]


def num_guessing(diff):
    if difficulty == 'hard':
        attempts = 5
    else:
        attempts = 10
    while attempts > 0:
        guess = int(input("Make a guess:"))
        if guess == number:
            print(f"You got it! The answer was {number}")
            return
        elif guess > number:
            attempts -= 1
            print(f"Too High.\nTry again.\nYou have {attempts} attempts remaining to guess the number.")
        elif guess < number:
            attempts -= 1
            print(f"Too Low.\nTry again.\nYou have {attempts} attempts remaining to guess the number.")

    print("You lose. No more attempts left")


print("Welcome to the Number Guessing Game!\nI'm thinking of a number between 1 and 100.")
number = random.choice(NUMBERS)
difficulty = input("Choose a difficulty. Type 'easy' to get 10 attempts or 'hard' for 5:")
num_guessing(difficulty)
