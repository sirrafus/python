# it's a common practice to use all uppercase on constants - vars that most probably will not be changed
# it's easy to see it's all caps and be careful not to modify it during the code execution
# Python there is no block scope. Inside a if/else/for/while code block is the same as outside it.
# that means if block or for or while loops won't count as a fence, so vars will be still available outside 
PI = 3.14159
