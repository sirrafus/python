import random

EASY_LEVEL = 10
HARD_LEVEL = 5


# function to check user's guess against actual number'
def answer_check(guess, answer, turns):
    """Check the answer vs guess, returns turns remaining"""
    if guess > answer:
        print("Too high")
        return turns - 1
    elif guess < answer:
        print("Too low")
        return turns - 1
    else:
        print(f"You got it! The answer was {answer}")


def set_diff():
    difficulty = input("Choose a difficulty. Type 'easy' to get 10 attempts or 'hard' for 5:")
    if difficulty == "hard":
        return HARD_LEVEL
    else:
        return EASY_LEVEL


def game():
    print("Welcome to the Number Guessing Game!\nI'm thinking of a number between 1 and 100.")
    answer = random.randint(1, 100)
    print(answer)
    turns = set_diff()
    guess = 0
    while guess != answer:
        print(f"You have {turns} attempts remaining.")
        guess = int(input("Make a guess:"))
        turns = answer_check(guess, answer, turns)
        if turns == 0:
            print("Out of guesses, you lose.")
            return
        elif guess != answer:
            print("Guess Again.")


game()


