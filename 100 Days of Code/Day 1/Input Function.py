print('What is your name')
"""
as the whole block in the print should be evaluated before printing it out steps go like following
1 - Hello is converted to string
2 - input gets requested
3 - input gets entered
4 - inputted replaces the input("text") part with actual input
5 - gets concatenated with Hello, gets concatenated with !
6 - printed out all together
"""
print("Hello " + input("What is your name? ") + "!")
name = input('What is your name \n')
print("Your name is", name)

# task No 3
print(len(input("What is your name?")))
