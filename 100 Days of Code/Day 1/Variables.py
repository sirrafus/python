# variables help to store data and inputs, and you can get to value with it's name
name = input("what is your name")
print(name)

name = "Angela"
print(name)

# same as print(len(input("What is your name?"))) using vars
name = input("WHat is your name?")
length = len(name)
print(length)

# task
# 🚨 Don't change the code below 👇
a = input("a: ")
b = input("b: ")
# 🚨 Don't change the code above

####################################
# Write your code below this line
# switching the variables
c = a
d = b
a = d
b = c

# or even better, with switching milk and tea using additional glass analogy

c = a
a = b
b = c

# Write your code above this line
####################################

# 🚨 Don't change the code below
print("a: " + a)
print("b: " + b)

# var names should make sense and be easy to understand even 6 month later
# can have underscores letters and numbers (nums are only in the end)
# should not have built in names
