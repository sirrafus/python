print("Day 1 - Python Print Function")
print("The function is declared like this:")
# different types of quotes are used to avoid getting wrong quoting, otherwise (what to print will be out of quotes)
print("print('what to print')")
# \n means new line
print("Hello World!\nHello "
      "World\nHello World!")

# Strings can be concatenated

print("hello" + "Tigran")
# adding space
print("hello" + " " + "Tigran")
