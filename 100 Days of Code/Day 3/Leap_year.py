# Don't change the code below
year = int(input("Which year do you want to check? "))
# Don't change the code above
# https://bit.ly/36BjS2D
# Write your code below this line
if year % 4 == 0:
    if year % 100 == 0:
        if year % 400 == 0:
            print("Leap year.")
        else:   # because if it matches first two conditions but doesn't match 3rd one it's not leap
            print("Not leap year.")
    else:  # because if it matches 1st condition but not the % 100 condition, it's leap
        print("Leap year.")
else:   # because if it fails condition 1 it's not a leap year for sure
    print("Not leap year.")
