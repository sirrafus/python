# real life example of if else is like this
# if bathtub is filled more than 80cm (where overflow is located) drain it until overflow level is reached again,
# else fill it more
# in python it's the following structure
"""
if condition:
    do this
else:
    do this
"""

print("Welcome to the roller coaster")
height = int(input("What is your height in cm? "))
# after if indentation is automatic as that means it comes AFTER the condition is met, child process
if height >= 120:
    print("You can ride")
# if and else are a pair that's why they should be on same level
else:
    cm_to_grow = 120 - height
    print(f"can't ride until you will grow for at least {cm_to_grow} cm. Eat more you skinny fuck!")