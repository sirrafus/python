# Don't change the code below
print("Welcome to the Love Calculator!")
name1 = input("What is your name? \n")
name2 = input("What is their name? \n")
#  Don't change the code above

# Write your code below this line
text = (name1 + name2).lower()
t = (text.count('t'))
r = (text.count('r'))
u = (text.count('u'))
e = (text.count('e'))
L = (text.count('l'))    # avoiding ambiguous warning in pycharm
o = (text.count('o'))
v = (text.count('v'))
e2 = (text.count('e'))

true = str(t + r + u + e)
love = str(L + o + v + e2)
score = int(str(true) + str(love))

if (10 > score) or score > 90:  # you can add parentheses to visually separate them, but it's not required
    print(f"Your score is {score}, you go together like coke and mentos.")
elif 40 <= score <= 50:
    print(f"Your score is {score}, you are alright together.")
else:
    print(f"Your score is {score}.")
