print('''
*******************************************************************************
          |                   |                  |                     |
 _________|________________.=""_;=.______________|_____________________|_______
|                   |  ,-"_,=""     `"=.|                  |
|___________________|__"=._o`"-._        `"=.______________|___________________
          |                `"=._o`"=._      _`"=._                     |
 _________|_____________________:=._o "=._."_.-="'"=.__________________|_______
|                   |    __.--" , ; `"=._o." ,-"""-._ ".   |
|___________________|_._"  ,. .` ` `` ,  `"-._"-._   ". '__|___________________
          |           |o`"=._` , "` `; .". ,  "-._"-._; ;              |
 _________|___________| ;`-.o`"=._; ." ` '`."\` . "-._ /_______________|_______
|                   | |o;    `"-.o`"=._``  '` " ,__.--o;   |
|___________________|_| ;     (#) `-.o `"=.`_.--"_o.-; ;___|___________________
____/______/______/___|o;._    "      `".o|o_.--"    ;o;____/______/______/____
/______/______/______/_"=._o--._        ; | ;        ; ;/______/______/______/_
____/______/______/______/__"=._o--._   ;o|o;     _._;o;____/______/______/____
/______/______/______/______/____"=._o._; | ;_.--"o.--"_/______/______/______/_
____/______/______/______/______/_____"=.o|o_.--""___/______/______/______/____
/______/______/______/______/______/______/______/______/______/______/_____ /
*******************************************************************************
''')
print("Welcome to Treasure Island.")
print("Your mission is to find the treasure.")

# Write your code below this line
crossroad = input('You are at a cross road. where do you want to go? Type "left" or right"\n').lower()

if crossroad == "left":
    lake = input('You came to a lake. There is an island in the middle of the lake. Type "wait" to wait for a boat.'
                 'Type "swim" to swim across.\n').lower()
    if lake == "wait":
        door = input('You arrived at the island unharmed. There is a house with 3 doors. One red, yellow and the blue. '
                     'Which color do you choose?\n').lower()
        if door == "yellow":
            print("You win, congarjulayshins!")
        elif door == "red":
            print("You were burned to an extra cwispy condition. Game over")
        elif door == "blue":
            print("You were eaten by beasts. Game Over")
        else:
            print("Something jumped from the darkness and you die. Game Over")
    else:
        print("You've chosen a door that doesn't exist. Game over")
else:
    print("you fall into a hole. Game over")
