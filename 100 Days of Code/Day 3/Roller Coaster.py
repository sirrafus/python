# unlike if lif else, where only one of the conditions will be matched and executed, multiple if should match all if
# statements that were given
height = float(input("Please provide your height in meters separated by dot:\n"))
# main if, goes to else if less than 120 ибо нехуй
if height > 1.2:  # check other conditions for final bill
    age = int(input("Please enter your age in years:\n"))
    ride_bill = 0
    if age < 12:
        ride_bill = 5
        print(f"Child ticket for ${ride_bill}")
    elif age < 18:
        ride_bill = 7
        print(f"Youth ticket for ${ride_bill}")
    elif 45 <= age <= 55:
        print(f"Free ride, all is well, don't worry!")
    else:
        ride_bill = 12
        print(f"Adult ticket for ${ride_bill}")
    # adding one more if, to be necessarily checked after age if is done.
    photo = input("Do you want to add photo for $3? Y or N\n")
    if photo == "Y":
        ride_bill += 3
        # if you print line below it will be executed only of photo == Y is true
    print(f"Total final bill would be ${ride_bill}")
else:
    print("Too short, eat some carrot you dangrat")
