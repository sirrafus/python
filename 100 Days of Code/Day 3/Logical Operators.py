# and means both conditions SHOULD be true to get true
# or means one of conditions should be true to get true both should be false to get false
# not X - reverses condition - true to false,  false to true
a = 12
print(a < 13 and a > 10)
print(13 > a > 15)
b = a > 10 or a > 13
print(b)
print(not a > 20)