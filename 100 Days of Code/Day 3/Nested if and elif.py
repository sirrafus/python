# in nested statements if first condition of if is passed, another one can be tested with it's own else
# for example height of someone matches first if and is enough to sell him a park ticket, but if on top of that
# he is 18 or younger, we need to give him discount
# so 1st condition is height and second one is age

# to go to outer else it should fail first if
# to go to inner else if should pass first if, but fail the second one
# to do what's in the inner if, it needs to pass both ifs

height = float(input("Please enter your height in meters:\n"))

if height >= 1.2:
    print("Can take a ride")
    age = int(input("Please enter your age:\n"))
    if age < 12:
        print("Cheapest ticket price of 5$!")
        # no need to specify over 12 in elif as to get to elif it should meet criteria of being not less than 12
    elif age <= 18:
        print("Please pay discounted price of 7$.")
    elif age <= 25:
        print("Please pay discounted price of 10$.")
    else:
        print("Regular price 12$")
else:
    print("can't ride, grow the fuck up")
