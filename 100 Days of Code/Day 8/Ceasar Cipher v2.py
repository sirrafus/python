alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z']
# you can add symbols to alphabet at shift between those in the same way as letters
switch = True
direction = ""


def caesar(text, shift, cipher_direction):
    output = ""
    shift %= 26
    if cipher_direction == "encode":
        for letter in text:
            if letter in alphabet:
                position = alphabet.index(letter)
                new_position = position + shift
                if position + shift > 25:
                    new_letter = alphabet[new_position - 26]
                    output += new_letter
                else:
                    new_letter = alphabet[new_position]
                    output += new_letter
            else:
                output += letter
        print(f"The encoded text is {output}")

    elif cipher_direction == "decode":
        for letter in text:
            if letter in alphabet:
                position = alphabet.index(letter)
                new_position = position - shift
                if position + shift < 0:
                    new_letter = alphabet[-26 + shift - 1]
                    output += new_letter
                else:
                    new_letter = alphabet[new_position]
                    output += new_letter
            else:
                output += letter
        print(f"The decoded text is {output}")


while switch:
    inside_switch = True
    while inside_switch:
        direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n")
        if direction == "encode" or direction == "decode":
            inside_switch = False
        else:
            print(f"{direction} is invalid input, try again...")

    text_to_process = input("Type your message:\n").lower()
    shift_to_process = int(input("Type the shift number:\n"))

    caesar(text=text_to_process, shift=shift_to_process, cipher_direction=direction)
    again = input("Type 'yes' to try again. Otherwise type 'no'.").lower()
    if again == "yes":
        switch = True
    elif again == "no":
        print("End of session.")
        switch = False
    else:
        print("End of session.")
        switch = False
