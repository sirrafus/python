# Write your code below this line
def prime_checker(number):
    if number <= 1:
        print("It's not a prime number.")
    elif number == 2 or number == 3 or number == 5:
        print("It's a prime number.")
    elif number % 2 == 0:
        print("It's not a prime number.")
    elif number % 3 == 0:
        print("It's not a prime number.")
    else:
        print("It's a prime number.")


def prime_checker2(number):
    if number > 1:
        for i in range(2, int((number/2)+1)):  # up to but not included itself, int to avoid floats
            if (number % i) == 0:
                print("It's not a prime num.")
                break
        else:
            print("It's a prime num.")
    else:
        print("It's not a prime num.")


def prime_checker3(number):
    is_prime = True
    if number <= 1:
        is_prime = False
    for i in range(2, number):
        if number % i == 0:
            is_prime = False
    if is_prime:
        print("Is a prime")
    else:
        print("Not a prime")
# Do NOT change any of the code below


while True:
    n = int(input("Check this number: "))
    prime_checker3(number=n)
