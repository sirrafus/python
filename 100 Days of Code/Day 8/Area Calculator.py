# Write your code below this line
import math


def paint_calc(height, width, cover):
	result = math.ceil((height * width) / cover)
	# another way to round up is to % to 1 and add 1 if it's  not 0
	# another interesting way is to add True value which is same as 1
	# int(21 / 5) + (21 % 5 > 0) it's 4 + 1 in this case and will remain 4 + 0 if there is 0 after modulo
	print(f"You will need {result} cans of paint to cover the area")


# Write your code above this line
# Define a function called paint_calc() so that the code below works.

# Don't change the code below

# as we are giving variables as arguments we can use positional arguments in function -
# they will be placed in the right places anyway
test_h = int(input("Height of wall: "))
test_w = int(input("Width of wall: "))
coverage = 5
paint_calc(height=test_h, width=test_w, cover=coverage)
