from art import logo
print(logo)
alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p',
            'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']


def caesar(start_text, shift_amount, cipher_direction):
    shift_amount %= 26
    end_text = ""
    if cipher_direction == "decode":
        shift_amount *= - 1
    for letter in start_text:
        if letter in alphabet:
            position = alphabet.index(letter)
            #  if it's encoding, if it will not work and will simply add position to shift
            # but if it's decode, it will be multiplied by -1 and reverse it's value and basically subtract that value
            #  encode 5 and add 5 will just 5 + 5 and decode will multiply second 5 with -1 and get -5 so 10 - 5 = 5
            new_position = position + shift_amount
            end_text += alphabet[new_position]
        else:
            end_text += letter
    print(f"The {cipher_direction}d text is {end_text}")


# caesar(start_text=text_to_process, shift_amount=shift_to_process, cipher_direction=direction)

# achieved while loop using function called looper
# again = input("Type 'yes' to try again. Otherwise type 'no'.").lower()
# if again == "no":
#     print("End of session.")
# elif again != 'yes':
#     print("End of session.")
#
#
# def looper(yes_or_no):
#     while yes_or_no == 'yes':
#         local_direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n")
#         local_text_to_process = input("Type your message:\n").lower()
#         local_shift_to_process = int(input("Type the shift number:\n"))
#         caesar(start_text=local_text_to_process,shift_amount=local_shift_to_process,cipher_direction=local_direction)
#         local_again = input("Type 'yes' to try again. Otherwise type 'no'.").lower()
#         if local_again == "no":
#             print("End of session.")
#             yes_or_no = 'no'
#         elif local_again != 'yes':
#             print("End of session.")
#             yes_or_no = 'no'
#
#
# looper(yes_or_no=again)

should_end = False
while not should_end:
    direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n")
    text_to_process = input("Type your message:\n").lower()
    shift_to_process = int(input("Type the shift number:\n"))
    shift_to_process %= 26
    caesar(start_text=text_to_process, shift_amount=shift_to_process, cipher_direction=direction)

    restart = input("Type 'yes' to try again. Otherwise type 'no'.\n").lower()
    if restart == 'no':
        should_end = True
        print("Goodbye")
    elif restart != 'yes':
        print("Invalid input, exiting")
        should_end = True
