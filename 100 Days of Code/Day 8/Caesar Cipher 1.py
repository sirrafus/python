alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
            'v', 'w', 'x', 'y', 'z']
direction = ""
switch = True
while switch:
    direction = input("Type 'encode' to encrypt, type 'decode' to decrypt:\n")
    if direction == "encode" or direction == "decode":
        switch = False
    else:
        print(f"{direction} is invalid input, try again...")

text = input("Type your message:\n").lower()
shift = int(input("Type the shift number:\n"))


def encrypt(plain_text, shift_amount):
    cipher_text = ""
    for letter in plain_text:
        if letter in alphabet:
            position = alphabet.index(letter)
            new_position = position + shift_amount
            if position + shift_amount > 25:
                new_letter = alphabet[new_position - 26]
                cipher_text += new_letter
            else:
                new_letter = alphabet[new_position]
                cipher_text += new_letter
        else:
            cipher_text += letter
    print(f"The encoded text is {cipher_text}")


def decrypt(encoded_text, shift_back_amount):
    clean_text = ""
    for letter in encoded_text:
        if letter in alphabet:
            position = alphabet.index(letter)
            new_position = position - shift_back_amount
            if position + shift_back_amount < 0:
                new_letter = alphabet[-26 + shift_back_amount - 1]
                clean_text += new_letter
            else:
                new_letter = alphabet[new_position]
                clean_text += new_letter
        else:
            clean_text += letter
    print(f"The decoded text is {clean_text}")


if direction == "encode":
    encrypt(plain_text=text, shift_amount=shift)
elif direction == "decode":
    decrypt(encoded_text=text, shift_back_amount=shift)

# again = input("Type 'yes' to try again. Otherwise type 'no'.").lower()
# if again == "yes":
#
# else:
#     print("End of session.")

