# Review:
# Create a function called greet().
# Write 3 print statements inside the function.
# Call the greet() function and run your code.

# in parentheses, we are creating variables and when passing something during function call,
# we are assigning that something to variable
def greet(name, tramadrutyun, answer):
	print(f"Barev {name}")
	print("vonc es?")
	print("lav yeghanak a che?")


# function that allows input:
# function argument is the actual piece of data that is being passed over to function, and parameter is the name of that
# data that we will use to refer to that data

def greet_with_name(name):
	print(f"nice to meet you, {name}!")
	print(f"What's up, {name}?")


greet_with_name("Armen")


def greet_with(name, location):
	print(f"Hello, {name}, where are you from?")
	print(f"Oh so, you're from {location}")


# following function uses 2 positional argument which don't know what your input is, they just pass it to the
# position of that var. If you input yerevan first it simpy say hello Yerevan


greet_with("Tigran", "Yerevan")


def greet_with_keywords(name, location):  # with the = sign you are ordering arguments and no matter of called
	#  order you are assigning them to correct argument
	# if you = while creating function, it is kinda default value given, otherwise use = when passing arguments to params
	print(f"Hello, {name}, where are you from?")
	print(f"Oh so, you're from {location}")


greet_with_keywords(location="Yerevan", name="Klep")


def num_keywords(a=1, b=2, c=3):
	print(f"{a, b, c}")


num_keywords(b=10, a=3, c=15)

