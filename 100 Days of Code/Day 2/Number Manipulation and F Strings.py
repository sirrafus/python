# convertation to int simply cuts of the last part
# following will give 2 instead of 3
print(int(8 / 3))
# you should use round to get more accurate results
print(round(8 / 3))
# getting last 3 decimal places
print(round(8 / 3, 3))

# floor division returns integer that is rounded down, 2 in this case. Even type will be integer
print(8 // 3)
print(type(8 // 3))
# even on clean divisions result will be float number
print(4 / 2)

result = (4 / 2)
# you can continue performing operations on same variable using mathematical sign and =
# for example you got 2 for result, but now you want to divide that once again with 2
result /= 2
print(result)
# it's useful for example to keep a score
score = 0
# instead of adding score like this - score = score + 1 you can use shorthand
score += 1
# you can -= *= += /= etc

# F Strings
# As we remember we can't concatenate strings with numbers, so we had to convert number part to string
# however that's not the case with F strings as it simply uses variables in the spaces you provide
# old version
print("your score is" + (str(score)))

# better version with f string
score = 0
height = 1.8
isWinningSon = True
print(f"Your score is: {score} and your height is {height} and you are winning is {isWinningSon}")

# Task
# 🚨 Don't change the code below 👇
age = input("What is your current age?")
# 🚨 Don't change the code above 👆

#Write your code below this line 👇
age_as_int = int(age)
max_age = 90
year_days = 365
weeks_in_year = 52
months_in_year = 12
current_age_days =  age_as_int * year_days
maximum_age_in_days = max_age * year_days
total_days_left = maximum_age_in_days - current_age_days
total_weeks_left = round(total_days_left / year_days * weeks_in_year)
total_months_left = round(total_days_left / year_days * months_in_year)

print(f"You have {total_days_left} days, {total_weeks_left} weeks, and {total_months_left} months left.")

# better way to complete it is to get years remaining and calculate based on it

# 🚨 Don't change the code below 👇
age = input("What is your current age?")
# 🚨 Don't change the code above 👆

#Write your code below this line 👇
age_as_int = int(age)
years_remaining = 90 - age_as_int
total_days_left = years_remaining * 365
total_weeks_left = years_remaining * 52
total_months_left = years_remaining * 12

print(f"You have {total_days_left} days, {total_weeks_left} weeks, and {total_months_left} months left.")