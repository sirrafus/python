# adding + sign
# subtracting - sign
# multiplication * sign
# division /
# dvision is always a float as a result
# floor division returns integer that is rounded down with // sign
print(9/3)
# exponents (raising to power) ** sign
print(2**3)

# Priorities for operations
# Rule PEMDAS - Parentheses, exponents, multiplication, division, addition, subtraction () ** * / + -
# however * with the / and + with the - are equal to each other and calculated by order from left to right

# result of the following will be a float as there is one float value
print(3 * 3 + 3 / 3 - 3)
# to get result 3
print(3 * (3 + 3) / 3 - 3)

# Task
# Don't change the code below
height = input("enter your height in m: ")
weight = input("enter your weight in kg: ")
# Don't change the code above

# Write your code below this line
# as we will print with float, converting that into regular int is not possible
height_int = float(height)
weight_int = int(weight)
print(int(weight_int / height_int ** 2))
# obviously you can create bmi var and store calculation line in it then just print var
