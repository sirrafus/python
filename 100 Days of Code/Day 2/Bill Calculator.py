# If the bill was $150.00, split between 5 people, with 12% tip.

# Each person should pay (150.00 / 5) * 1.12 = 33.6
# Format the result to 2 decimal places = 33.60

# Tip: There are 2 ways to round a number. You might have to do some Googling to solve this.

# Write your code below this line

print("Welcome to the tip calculator, niggaz!")
total_usd = float(input("What was the total bill? $"))
tip_percentage = int(input("What percentage tip you would like to give? 10, 12 or 15? "))
# acceptable_percentage = [10, 12, 15]
# while True:
#     percentage = eval(str(tip_percentage))
#     if percentage in acceptable_percentage:
#         break
#     else:
#         print("please input only 10, 12 or 15")
tip_percentage2 = float(str(1) + '.' + str(tip_percentage))
total_people = int(input("How many people to split the bill? "))
percent = float("1" + "." + str(tip_percentage))
bill_per_person = round((total_usd * tip_percentage / 100 + total_usd) / total_people, 2)
bill_per_person2 = round(total_usd * tip_percentage2 / total_people, 2)
bill_per_person3 = round(total_usd / total_people * percent, 2)
bill_per_person4 = round(total_usd * (1 + tip_percentage / 100) / total_people, 2)
# next line is giving the format that should be applied to given value or var
# basically it says add 2 floating (2f) after point
print(f"Each person should pay: ${bill_per_person}")
print(f"Each person should pay: ${bill_per_person2}")
print(f"Each person should pay: ${bill_per_person3}")
# to get exactly 2 decimal places for var even if second one is 0 use :.xf where x is amount of mandatory decimals
print(f"Each person should pay: ${bill_per_person4:.2f}")

# another way of doing it
print("Welcome to the tip calculator!")
bill = float(input("What was the total bill? $"))
tip = int(input("What percentage tip you would like to give? 10, 12 or 15? "))
people = int(input("How many people to split the bill? "))
# because any number times 0.xx equals xx percent of that number
tip_percent = tip / 100
total_tip = bill * tip_percent
total_bill = bill + total_tip
bill_for_person = total_bill / people

# .format is creating string value
final_amount_with_decimals = "{:.2f}".format(bill_for_person)
print(f"Each person should pay: ${final_amount_with_decimals}")