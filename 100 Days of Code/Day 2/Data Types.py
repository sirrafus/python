"""
there are various data types:
Strings
Integers
Float
Boolean
Ranges
"""

# Strings are text strings and are indexed, and we can pull out each character individually, and is called subscripting
# indexes start from 0
print('Hello'[0])
print('Hello'[4])
print('Hello'[-1])
# you can't get len for numbers, but you can convert them to strings and get it
# obviously numbers converted to string ar treated as a string
print("123" + "345")

# Integers are whole numbers
print(123 + 456)

# you can split large numbers with underscores
print(123_456_789)

# Floating point numbers are numbers with decimal points even .0
print(3.14159)

# Boolean only True or False
True
False

# you can't concatenate strings with integers
# if you want to check what type is the object use type() function
num_char = len(input("What is your name?\n"))
print(type(num_char))

# we can convert data types
str_char = str(num_char)
print(type(str_char))
# now we can concatenate the user input with other strings
print("Your name has " + str_char + " characters in it")

# will print sum of numbers as a float
print(70 + float(100.5))
# will print just 700100
print(str(70) + str(100))

# task
# Don't change the code below
two_digit_number = input("Type a two digit number: ")
# Don't change the code above

####################################
# Write your code below this line
# note that by default input was treated as string (as it's more universal in general), so we need to convert it to int
# you can set 2 additional vars to get values from subscripting and use those in + if you want
print(int(two_digit_number[0]) + int(two_digit_number[1]))

# ranges use syntax of start num, en num (not included) step(optional)
# step means + x each time where x is step and first number can be omitted and will be treated by python as 0
# so if we want to get all evens from 0 to 10 we ca have range(11, 2)
