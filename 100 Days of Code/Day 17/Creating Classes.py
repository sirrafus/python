# initialising is when you set variables and counters to their stating values
# you initialise python class with special function __init__
# Class is the blueprint to create custom objects
# __init__ will be called every single time class called to create an object
#  self is the actual object itself that is being created

class User1:

    def __init__(self):
        print("New user is being created")


# creating object from a class
user1 = User1()
# creating attributes for a class (variables attached to object)
user1.id = "001"
user1.username = 'Tigran'

print(f"{user1.id} {user1.username}")

user2 = User1()
user2.id = "002"
user2.username = "Ruzanchiklare"


# attributes are variables that are associated with an object
# car object has seats = 5 (seats = attrib), 4 wheels etc

class Car:
    def __init__(self, seats):
        self.seats = seats

    def enter_race_mode(self):
        self.seats = 2

# once you set this, my_car.seats will be equal to 5

# this is same as you leave self.seats in function, but it's way more efficient and less error-prone, compared to
# having it in the function as attribute.
# However, when you create attributes you are saying that new object created from it MUST have that parameters


my_car = Car(5)
print(my_car.seats)


class Car2:
    def __init__(self):
        pass


my_car2 = Car2()
my_car2.seats = 5
print(my_car.seats, my_car2.seats)


class User:
    def __init__(self, user_id, username):
        self.id = user_id                   # you can nam attrib different from parameter
        self.username = username            # but usually it's convention they are the same, just with self
        self.followers = 0                  # instead of setting followers to variable we give it the default value
# in such cases you can miss that parameter;s value, and it will be set to given value by default
# it's there, and we can change it later or call it, but object creation will not require it
        self.following = 0

    def follow(self, user):                 # user we decided to follow
        user.followers += 1                 # when we start to follow a user, he got a follower
        self.following += 1                 # we got following


user3 = User("003", "Gvochi")
user4 = User("004", "mard")

# let's say user 3 wants to follow user 4. his following will be added by 1 and user3's followers also by 1

user3.follow(user4)
print(user4.followers, "followers")
print(user3.following, "following")
print(user4.following, "following")
print(user3.followers, "followers")

print(user3.username)
print(user3.followers)
user3.followers = 15
print(user3.followers)


# adding methods to class (things that object can do) which are functions
# let's say we want a race mode for our car where weight is important,
# so we drop second row of the seats. we create method called enter_race_mode():
# unlike regular functions methods MUST have self parameter

my_car3 = Car(5)

my_car3.enter_race_mode()
print(my_car3.seats)