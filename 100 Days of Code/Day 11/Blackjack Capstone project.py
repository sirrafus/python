import random

cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
start = (input("Do you want to play a game of Blackjack? Type 'y' or 'n':"))

# Initial Part
player_initial = []
for card in range(0, 2):
    player_initial.append(random.choice(cards))
sum_of_player = sum(player_initial)
print(f"Your Cards: {player_initial}, current score: {sum_of_player}")

dealer_initial = []
for card in range(0, 22):
    dealer_initial.append(random.choice(cards))
sum_of_dealer = sum(dealer_initial)

dealer_first_card = dealer_initial[0]
print(f"Computer's First card: {dealer_first_card}")


if dealer_initial == 21:
    print("Dealer got blackJack, dealer wins")
elif player_initial == 21:
    print("You got blackJack, dealer wins")
else:
    more = input("Type 'y' to get another card, type 'n' to pass:")
    if more == 'y':
        player_initial.append(random.choice(cards))
