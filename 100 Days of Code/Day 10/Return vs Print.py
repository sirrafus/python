def add(n1, n2):
    return n1 + n2


def subtract(n1, n2):
    return n1 - n2


def multiply(n1, n2):
    return n1 * n2


def divide(n1, n2):
    return n1 / n2


operations = {
    "+": add,
    "-": subtract,
    "*": multiply,
    "/": divide,

}

num1 = int(input("What's the first number?\n"))
for operation in operations:
    print(operation)
operation_symbol = input("Pick an operation from the line above:\n")
num2 = int(input("What's the second number?\n"))
calculation_function = operations[operation_symbol]
first_answer = calculation_function(num1, num2)

print(f"{num1}{operation_symbol}{num2} = {first_answer}")

operation_symbol = input("Pick another operation\n")
num3 = int(input("What's the third number?\n"))
calculation_function = operations[operation_symbol]
second_answer = calculation_function(calculation_function(num1, num2), num3)  # same as passing first_answer as 1st arg
# there is only 1 calc function, but it used twice after, so it will create a bug and use last chosen operator
# on all 3 numbers you inputted
print(second_answer)
print(f"{first_answer}{operation_symbol}{num3} = {second_answer}")
