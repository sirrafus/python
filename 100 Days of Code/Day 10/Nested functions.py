# outer takes 5 and 10 does nothing with it just keeps
# inner just does c + d, remembers it  as there is nothing to show
# outer during his return passes arguments to inner and replaces c +d (which were added to each other and
# automatically gets 15 as a result
test = input(int("input the number"))

# def outer_function(a, b):
#     def inner_function(c, d):
#         return c + d
#     return inner_function(a, b)
#
#
# result = outer_function(5, 10)
# print(result)

print(test)