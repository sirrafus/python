should_stop = False
x = 0


def is_leap(year):
    if year % 4 == 0:
        if year % 100 == 0:
            if year % 400 == 0:
                return True
            else:
                return False
        else:
            return True
    else:
        return False


def days_in_month(year, month):
    """ Shows days for any year
    and any month"""
    if month not in range(1, 13) or month == "":
        return "Invalid Month"
    month_days = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    month_adjuster = month_days[month - 1]
    if year == "":
        return "invalid input"
    elif is_leap(year) and month == 2:
        return month_adjuster + 1
    else:
        return month_adjuster


# Do NOT change any of the code below
while not should_stop:
    year = int(input("Enter a year: "))
    month = int(input("Enter a month: "))
    days = days_in_month(year, month)
    print(days)
    loop = input("Another one? y or n")
    if loop == "y":
        print("Let's Continue")
        should_stop = False
    elif loop == "n":
        print("Breaking...")
        should_stop = True
    else:
        print("WTF was that? Breaking...")
        should_stop = True

days_in_month()