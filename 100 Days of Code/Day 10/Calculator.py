from art import calc

print(calc)
should_stop = False
answer = None

def add(n1, n2):
    return n1 + n2


def subtract(n1, n2):
    return n1 - n2


def multiply(n1, n2):
    return n1 * n2


def divide(n1, n2):
    return n1 / n2


operations = {
    "+": add,
    "-": subtract,
    "*": multiply,
    "/": divide,

}


while not should_stop:
    if answer is None:
        num1 = int(input("What's the next number?\n"))
        for operation in operations:
            print(operation)
    else:
        num1 = int()
        num1 += answer
    operation_symbol = input("Pick an operation.:\n")
    num2 = int(input("What's the next number?\n"))
    answer = operations[operation_symbol](num1, num2)
    # or you can create a var like this:
    # calculation_function = operations[operation_symbol]
    # and call it
    # answer = calculation_function(num1, num2)
    print(f"{num1}{operation_symbol}{num2} = {answer}")
    should_continue = input(f"Type 'y' to continue calculating with {answer} or type 'n' to exit.:")
    if should_continue == 'y':
        print("Continuing")
    else:
        print("Goodbye")
        should_stop = True
