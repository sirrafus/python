# return is for output for function
# like empty bottle goes to some machine and exits after some processes with the milk in
# return will replace the function with returned result whenever function was called
# if there are more than one values given to return it returns them in tuple
# code added after return (next line) will be unreachable because return means end of function


def my_function():
    result = 3 * 4
    return result


output = my_function()
print(output)


def format_name(f_name, l_name):
    if f_name == "" or l_name == "":
        return "Wrong input"  # will break the function on empty inputs
    title_f_name = f_name.title()
    title_l_name = l_name.title()
    return f"{title_f_name} {title_l_name}", "test"


first_name = input("enter first name:")
last_name = input("enter last name:")

output = format_name(first_name, last_name)
print(output)

# you can put inputs in the called function directly instead of assigning it to var and call using var

output = format_name(input("enter first name:"), input("enter last name:"))

print(output)
