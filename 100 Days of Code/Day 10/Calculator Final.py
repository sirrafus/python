from art import calc


def add(n1, n2):
    return n1 + n2


def subtract(n1, n2):
    return n1 - n2


def multiply(n1, n2):
    return n1 * n2


def divide(n1, n2):
    return n1 / n2


operations = {
    "+": add,
    "-": subtract,
    "*": multiply,
    "/": divide,

}

# recursion
# function that calls itself:


def calculator():
    print(calc)
    should_continue = True
    num1 = int(float(input("What's the first number?\n")))
    for operation in operations:
        print(operation)

    while should_continue:
        operation_symbol = input("Pick an operation:\n")
        num2 = float(input("What's the next number?\n"))
        calculation_function = operations[operation_symbol]
        answer = calculation_function(num1, num2)
        print(f"{num1}{operation_symbol}{num2} = {answer}")
        again = input(f"Type 'y' to continue calculating with {answer} or type"
                      f" 'new' to start again or anything else to exit.:")
        if again == "y":
            num1 = answer
        elif again == 'new':
            should_continue = False
            calculator()    # recursion

        else:
            print("Goodbye")
            should_continue = False


calculator()
