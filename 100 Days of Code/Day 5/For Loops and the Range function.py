# numbers within range 1 to 10 or any other range is easier to calculate with the following logic:
# if you reverse the numbers and add with same position you will always get sum of first num + last num
# in this case it's 11. So we can simply do 11 x 10 x2 and get the same result
# 1+ 2 + 3 + 4 + 5 = 15
# 5 + 4 + 3 + 2 + 1 = 15

# range uses start and end (does not include mentioned number)

total = 0
for number in range(1, 101):
    print(number)
    total = total + number  # or += number

print(total)

# third option in range function is step
for number in range(1, 11, 2):  # starts from 1 but print every second number (3, 5 etc) it's basically +x  each time
    print(number)

