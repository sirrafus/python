# Write your code below this row
for number in range(1, 101):
    if number % 3 == 0 and number % 5 == 0:
        print("FizzBuzz")
    elif number % 3 == 0 and number % 5 > 0:
        print("Fizz")
    elif number % 3 > 0 and number % 5 == 0:
        print("Buzz")
    else:
        print(number)


# alternative way. Order if last 2 elifs doesn't matter as there is no OVERLAP
for number in range(1, 101):
    if number % 3 == 0 and number % 5 == 0:
        print("FizzBuzz")
    elif number % 5 == 0:
        print("Buzz")
    elif number % 3 == 0:
        print("Fizz")
    else:
        print(number)
