# Don't change the code below
student_scores = input("Input a list of student scores ").split()
for n in range(0, len(student_scores)):
    student_scores[n] = int(student_scores[n])
print(student_scores)
# Don't change the code above
# there is built in function max that finds the biggest value in the list, but we should not use it for this task

# Write your code below this row
highest_num = [0]   # you can use regular var and not list. set it to 0 and say if score > 0
# then highest_num equals score
for score in student_scores:
    if score > highest_num[0]:
        highest_num[0] = score
print(f"The highest score in the class is: {highest_num[0]}")

