# Don't change the code below
student_heights = input("Input a list of student heights ").split()  # note that split does not work with int
for n in range(0, len(student_heights)):  # does not include last number so no need to -1
    student_heights[n] = int(student_heights[n])
# Don't change the code above
# Write your code below this row
# there is built in functions max and len to achieve what's needed here, but we should not use it for this task
sum_of_heights = 0
qty_of_students = 0
for height in student_heights:
    sum_of_heights += height
    qty_of_students += 1
average_height = sum_of_heights / qty_of_students
print(sum_of_heights)
print(qty_of_students)
print(round(average_height))
