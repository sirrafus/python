# for item in list_of_items:
#   do something to each item
# it's a good practice to call singular version of whatever the plural version of var name is
fruits = ["Apple", "Peach", "Pear"]
for fruit in fruits:
    print(fruit)
    print(fruit + " Pie")