# unlike real life machines are deterministic, so by default they don't do random stuff
# Python uses Mersenne Twister PseudoRandom Number Generator (PRNG)
# importing random module, which is built in module
import random
# importing module that I manually created myself from the same folder
import variables

pi_value = variables.pi
print(pi_value)

# print random number between 1 and 200 including, which is integer
random_integer = random.randint(1, 200)
print(random_integer)

# you can import modules and variables and functions from them

# printing random float number from 0 to 1 not including 1
random_float = random.random()
print(random_float)

float_with_integer = random_integer + random_float
print(float_with_integer)
# or you can multiply random.random with whole number and get whole numbers with decimals, which are again random
# to get a whole number with decimal from 0.0000 to 4.99999999
whole_floating_random_number = random_float * 5
print(whole_floating_random_number)

# as all that love generator stuff is a pseudoscientific bullshit,
# we can create the same using much less code
score = random.randint(1, 100)
if (10 > score) or score > 90:
    print(f"Your score is {score}, you go together like coke and mentos.")
elif 40 <= score <= 50:
    print(f"Your score is {score}, you are alright together.")
else:
    print(f"Your score is {score}.")
