states = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida",
          "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine",
          "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska",
          "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio",
          "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas",
          "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]

print(states[49])
#  if we get len for states var and pass that as index, it will generate off by one type error
#  message is the same as out of index, but you can simply lower the value with 1 and avoid it
var_len = len(states)
print(states[var_len - 1])

# list can contain lists themselves

even_nums = [1, 3, 5, 7, 9]
odd_nums = [2, 4, 6, 8, 10]
nums = [even_nums, odd_nums]
print(nums)