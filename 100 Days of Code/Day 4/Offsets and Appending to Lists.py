# Lists are data structures
# lists always start and end with square brackets, items are separated with comma
# lists are ordered

fruits = ["cherry,", "apple", "pear"]

states = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida",
          "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine",
          "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska",
          "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio",
          "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas",
          "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]

# indexing starts from 0, like how many items is shifter from the start, 1st one is the start itself so 0
print(states[0])
print(fruits[1])
# you can also use negative indexes and -1 start counting from the end
print(states[-1])
print(states[-2])

# you can alter any list items
states[-1] = 'wyoming'
print(states[-1])
print(states)

# append function will add single item to the end of the list
fruits.append("Banana")
print(len(fruits))
print(fruits)

# length for list starts from 1 so basically if you use length to insert something, it will do the same as append
fruits.insert(len(fruits), "Marakuya")
print(fruits)

# you can extend list with other list and add it's items one by one
fruits.extend(["Lemon", "Orange"])
print(fruits)
# adding string to list will add all letters one by one
fruits += "banana"
print(fruits)