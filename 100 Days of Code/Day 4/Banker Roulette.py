# Don't change the code below 👇
import random
test_seed = int(input("Create a seed number: "))

# with the given seed random
random.seed(test_seed)

# Split string method
names_string = input("Give me everybody's names, separated by a comma. ")
names = names_string.split(", ")

# Don't change the code above

# Write your code below this line
# split separates item  using mentioned separator

name = names[random.randint(0, len(names) - 1)]

print(f"{name} is going to buy the meal today!")

# much easier way to do with builtin choice function method, comment everything but lines 9 and 10 and lines below

name = random.choice(names)
print(f"{name} is going to buy the meal today!")

