# Don't change the code below
# if you copy emojis, they look like empty squares in the IDE
row1 = ["⬜️", "⬜️", "⬜️"]
row2 = ["⬜️", "⬜️", "⬜️"]
row3 = ["⬜️", "⬜️", "⬜️"]
map1 = [row1, row2, row3]
print(f"{row1}\n{row2}\n{row3}")
position = input("Where do you want to put the treasure? ")
# Don't change the code above

# Write your code below this row
column = int(position[0])
row = int(position[1])

map1[row - 1][column - 1] = "X"

# alternative way
# selected_row = map1[column - 1] choosing first input 2 as a vertical index in the list
# this only means the position WITHIN the nested lists, but we haven't chosen list itself yet, this is crucial
# after saying it's the 1st index of whatever nested list,we are saying that that nested list is 3(2nd input of user) -1
# and replacing it with value X.
# so column -1 = whatever position in nested list, row -1 is whatever list it is in the main list
# selected_row[row -1] = "X"

# Write your code above this row

# Don't change the code below
print(f"{row1}\n{row2}\n{row3}")
