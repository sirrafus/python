# all dictionaries have key value pairs, separated by comma
# key - word, value - definition of the word
# good way of writing dictionary below:

programming_dictionary = {
    "Bug": "An error in a program that prevents the program from running as expected.",
    "Function": "A piece of code that you can easily call over and over again.",
    "Loop": "The action of doing something over and over again"

}
# calling certain key from dict
print(programming_dictionary["Bug"])

# adding to dict
# note that key just like index is within [], however value with [] will be just list type and not string
programming_dictionary["Debugging"] = "Process of going through the code to identify problems"

print(programming_dictionary)

empty_dictionary = {}

# You can wipe the dict by assigning empty value to it
# programming_dictionary = {}

programming_dictionary["Bug"] = "An error in a program, that prevents the program from running as expected."
print(programming_dictionary)

# in for loops it loops through all the keys and prints their names if nothing specified
# But if specified, prints those instead

for thing in programming_dictionary:
    print(thing)
    # you can  use that in [] to call value without asking for value explicitly
    print(programming_dictionary[thing])
print(len(programming_dictionary))