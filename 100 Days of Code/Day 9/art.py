"""
Regular methods automatically take instance (self) as a first argument
to make it take Class as instance we can create Class Methods

"""


class Employee:
    raise_amount = 1.04  # class variable.
    # Even within the class you should call it either by class name or instance name self
    num_of_employees = 0  # to count every employee that was created with a class


    @classmethod
    def from_string(cls, emp_str):  # alternative contractors usually have convention to start with "from"
        first, last, pay = emp_str.split("-")
        return cls(first, last, int(pay))

emp_str_2 = "Ruzanna-Melikyan-7800"
emp_str_3 = "Dishwasher-Mughdusyan-500"


# not exactly working as expected, will look into this later
new_emp_2 = Employee.from_string(emp_str_2)
new_emp_3 = Employee.from_string(emp_str_3)

print(new_emp_3)