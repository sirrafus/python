# you can nest lists in dicts and dicts in lists, with "t"
# lists in lists are not as useful as lists in dict or dicts in dicts
travel_log = {
    "France": {"cities_visited": ["Paris", "Little", "Dijon"], "total_visits": 12},
    "Germany": {"cities_visited": ["Stuttgart", "Munich", "Berlin"], "total_xaracho_times": [15, 5, 8]}

}

# nesting a dict inside a list
list_of_dicts = [
    {"country": "France",
     "cities_visited": ["Paris", "Little", "Dijon"],
     "total_times": 12
     },
    {"country": "Germany",
     "cities_visited": ["Stuttgart", "Munich", "Berlin"],
     "total_xaracho_times": [15, 5, 8]
     }

]

