from art import logo
print(logo)
print("Welcome to the secret auction program.")

should_stop = False
bidders = {}
max_key = ""
max_value = 0


while not should_stop:
    name = input("What is your name?: ")
    bid = int(input("what's your bid?: $"))
    bidders[name] = bid
    others = input("are there any other bidders? Type 'yes' or 'no'.")
    if others == 'yes':
        should_stop = False
        bidders[name] = bid

    else:
        should_stop = True
    for key in bidders:
        if max_value == 0 or max_value < bidders[key]:
            max_value = bidders[key]
            max_key = key

print(f"The winner is {max_key} with a bid of ${max_value}")

