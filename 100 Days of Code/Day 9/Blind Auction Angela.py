from art import logo

print(logo)
print("Welcome to the secret auction program.")

bidding_finished = False
bids = {}


def find_highest_bidder(bidding_record):
    max_key = 0
    max_value = 0
    for key in bidding_record:
        value = bidding_record[key]
        if value > max_value:
            max_value = value
            max_key = key
    print(f"The winner is {max_key} with a bid of ${max_value}")


while not bidding_finished:
    name = input("What is your name?: ")
    price = int(input("what's your bid?: $"))
    bids[name] = price
    others = input("are there any other bidders? Type 'yes' or 'no'.\n")
    if others == 'yes':
        bids[name] = price
    else:
        bidding_finished = True
        find_highest_bidder(bids)



